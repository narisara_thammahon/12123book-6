# 12123BOOK

**ขั้นตอนการติดตั้งโปรแกรม 12123BOOK**

1. ให้ทำการติดตั้ง xampp
2. ทำการดาวน์โหลดโฟลเดอร์ 12123book และทำการเซฟไว้ที่  C:\xampp\htdocs
3. เปิด xampp และกด Start Apache และ MYSQL 
4. เปิด web browser และพิมพ์ localhost
    *( หน้าเว็บจะเข้าไปที่หน้า localhost อัตโนมัต )*
5. เข้าไปที่ phpMyAdmin
6. สร้าง database และ import ไฟล์ database ที่ชื่อ bookonline.sql 
7. เปิดหน้าเว็บโดยการพิมพ์  localhost/12123book/index.php