<?php
    include('sever.php');
    session_start();

    if (isset($_SESSION['username'])) {
      header('location: /12123book/member.php');
    }

    $sqlbook1 = "SELECT DISTINCT Title,Price,Type FROM book ORDER BY BookID DESC LIMIT 16";
    $result1 = mysqli_query($conn, $sqlbook1);
   
    
    
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap');

     @font-face {
        font-family: 'Fjalla One', sans-serif;
    }
    body {
        font-family: 'Fjalla One', sans-serif;
    }
    </style>

</head>

<body  >

<div class="header">
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-blue border-bottom shadow-sm">
<h1 class="my-0 mr-md-5 " ><a href="/12123book/index.php" style="color: #ffffff" >12123BOOK</a></h1>

    <form class="form-inline my-0 mr-md-auto" action="/12123book/book_db.php" method="get" >
        <input class="form-control mr-sm-2" style="color: #3c763d" type="text" name="bookname" placeholder="Search" >
        <button class="btn btn-success my-1 my-sm-0" type="submit" id="search" name="search">Search</button> 
      </form>
      
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark"> </a>
    <a class="p-2 text-dark"> </a>
    <a href="/12123book/transfer.php"><input type="button" class="btn btn-outline-success" value="แจ้งโอนเงิน"></a>
  </nav>
    <a class="btn btn-success" href="/12123book/login_db.php">Login</a>
    <a class="p-2 text-dark" ></a>
    <a href="/12123book/register.php"><input type="button" class="btn btn-success" value="สมัครสมาชิก"></a>
    <a class="p-2 text-dark" ></a>
    <a class="py-2" href="/12123book/buy.php?text1=<?php echo $username ?>" aria-label="Product">
  <svg class="bi bi-bag" width="2em" style="color: #3c763d" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
    <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
  </svg></a>
</div>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-8">
    <a href="/12123book/type.php?text=นิยายไทย"><input type="button" class="btn btn-outline-success" value="นิยายไทย"></a>
    <a class="p-2 text-dark" ></a>
    <a href="/12123book/type.php?text=นิยายแปล"><input type="button" class="btn btn-outline-success" value="นิยายแปล"></a>
    <a class="p-2 text-dark" ></a>
    <a href="/12123book/type.php?text=การ์ตูน"><input type="button" class="btn btn-outline-success" value="การ์ตูน"></a>
    <a class="p-2 text-dark" ></a>
    <a href="/12123book/type.php?text=วรรณกรรม"><input type="button" class="btn btn-outline-success" value="วรรณกรรม"></a>
    <a class="p-2 text-dark" ></a>
    <a href="/12123book/type.php?text=สารคดี"><input type="button" class="btn btn-outline-success" value="สารคดี"></a>
    </div>
    <div class="col-sm-4"></div>
  </div>

  <div class="newbook">
    <h3 style="color: #3c763d">หนังสือใหม่</h3>
  </div>




<div class="container">
    <div class="book1">
    
        <div class="row row-cols-4">
        <?php if ($result1) { 
        while($record= $result1->fetch_assoc()) { ?>
            <div class="col" style="margin-top: 50px" >
                <a href="/12123book/book_db.php?text=<?php echo $record["Title"]; ?>"><img src = "/12123book/bookpic/<?php echo $record["Title"]; ?>.jpg" width="150" height="200"></a><br> <a  href="/12123book/book_db.php?text=<?php echo $record["Title"]; ?>"><?php echo $record["Title"]; ?><br>  ราคา: <?php echo $record["Price"]; ?> บาท</br></br></a>
                <small style="color: #003300">หมวด : <a style="color: #3c763d" href="/12123book/type.php?text=<?php echo $record['Type']; ?>"><?php echo $record['Type']; ?></a></small>
            </div>
        <?php }
        }else {
            echo "no";
        } ?>
        </div> 

</div>
</body>
</html>